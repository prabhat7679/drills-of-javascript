const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

// Find all users who are interested in playing video games.

function videoGame(){
    const usersPlayVideoGames = Object.keys(users).filter((user)=>{
        let interestOfUser = users[user].interests;
          
        if(interestOfUser === undefined){
            interestOfUser= users[user].interest;
        }

        interestOfUser = interestOfUser.join(',')
         return interestOfUser.includes("Video Games")
    })

    console.log(usersPlayVideoGames)
}
    
 videoGame()

function staying(){
    const stayIn= Object.keys(users).filter((user)=>{
         return users[user].nationality==='Germany';
    })
    console.log(stayIn)
}
//const stayIn= staying()

// Find all users with masters Degree.

function masters(){
    const mastersDegree= Object.keys(users).filter((user)=>{
        return users[user].qualification.includes('Master')
    })
    console.log(mastersDegree)
}
//const  Master = masters();

// Group users based on their Programming language mentioned in their designation.



