const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];
/*
    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 
//1. Get all items that are available 

function available(){
    const itemsAvailable = items.filter((item)=>{
        if(item.available === true){
            return item;
        }
    })
    return itemsAvailable;
}
const itemsAvg = available();
//  console.log(itemsAvg);

//    2. Get all items containing only Vitamin C.
function containVitaminC(){
    const itemsVitamin = items.filter((item)=>{
        if(item.contains.includes('Vitamin C')){
            return item;
        }
    })
    return itemsVitamin;
}
const VitaminC = containVitaminC();
//  console.log(VitaminC);
//  3. Get all items containing Vitamin A.

function containVitaminA(){
    const itemsVitamin = items.filter((item)=>{
        if(item.contains.includes('Vitamin A')){
            return item;
        }
    })
    return itemsVitamin;
}
const VitaminA = containVitaminA();
 // console.log(VitaminA)

 //    4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

function groupItems(){

    const group= items.reduce((accumulatorItem, CurrentItem)=>{
 
        
    },{})
   // console.log(group);
}
const group = groupItems();

//   Sort items based on number of Vitamins they contain.

function sortItemFn(){
    const sorted = items.sort((item1, item2)=>{
       const count1= item1.contains.split(', ').length;
       const count2= item2.contains.split(', ').length;

       return count2- count1;

    });
    return sorted;
}
const sortedItem = sortItemFn();
console.log(sortedItem);