items =[{"id":1,"card_number":"5602221055053843723","card_type":"china-unionpay","issue_date":"5/25/2021","salt":"x6ZHoS0t9vIU","phone":"339-555-5239"},
{"id":2,"card_number":"3547469136425635","card_type":"jcb","issue_date":"12/18/2021","salt":"FVOUIk","phone":"847-313-1289"},
{"id":3,"card_number":"5610480363247475108","card_type":"china-unionpay","issue_date":"5/7/2021","salt":"jBQThr","phone":"348-326-7873"},
{"id":4,"card_number":"374283660946674","card_type":"americanexpress","issue_date":"1/13/2021","salt":"n25JXsxzYr","phone":"599-331-8099"},
{"id":5,"card_number":"67090853951061268","card_type":"laser","issue_date":"3/18/2021","salt":"Yy5rjSJw","phone":"850-191-9906"},
{"id":6,"card_number":"560221984712769463","card_type":"china-unionpay","issue_date":"6/29/2021","salt":"VyyrJbUhV60","phone":"683-417-5044"},
{"id":7,"card_number":"3589433562357794","card_type":"jcb","issue_date":"11/16/2021","salt":"9M3zon","phone":"634-798-7829"},
{"id":8,"card_number":"5602255897698404","card_type":"china-unionpay","issue_date":"1/1/2021","salt":"YIMQMW","phone":"228-796-2347"},
{"id":9,"card_number":"3534352248361143","card_type":"jcb","issue_date":"4/28/2021","salt":"zj8NhPuUe4I","phone":"228-796-2347"},
{"id":10,"card_number":"4026933464803521","card_type":"visa-electron","issue_date":"10/1/2021","salt":"cAsGiHMFTPU","phone":"372-887-5974"}]

/* 

    1. Find all card numbers whose sum of all the even position digits is odd.
    2. Find all cards that were issued before June.
    3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
    4. Add a new field to each card to indicate if the card is valid or not.
    5. Invalidate all cards issued before March.
    6. Sort the data into ascending order of issue date.
    7. Group the data in such a way that we can identify which cards were assigned in which months.

    Use method chaining to solve #3, #4, #5, #6 and #7.

    NOTE: Do not change the name of this file 
*/

//   1. Find all card numbers whose sum of all the even position digits is odd.

function numbersumodd(){
    const cardNumber = items.filter((item)=>{
        let number = item.card_number;
        let numberArray=(number.split(''));
       // console.log(numberArray)
        //  "5602221055053843723"
        const sumOfEven = numberArray.reduce((number1, number2, index)=>{
            const number3=Number(number2);
            if(index %2 !==0){
                number1+=number3;
                
            }
 // console.log(number1);
            return number1;
        },0);

        if(sumOfEven%2 !==0){
     return item.card_number;
        }
    })
    return cardNumber;
}
const cardNumber =numbersumodd();
//  console.log(cardNumber);

//2. Find all cards that were issued before June.

function beforeJune(){
    const number = items.filter((item)=>{
        const date = item.issue_date.split('/');
        const month = date[0];
        if(month < 6){
            return month;
        }

    })
    return number;
}

const month = beforeJune();
// console.log(month);

//   3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
function newField(){
    const newAdd= items.map((item)=>{
        item['CVV']= Math.floor((Math.random()* 900 )+100);
        //return item.CVV;   
        return item; 
    }).map((item)=>{
            
        item['Valid']= true;
        return item;
    }).map((item)=>{
        const date = item.issue_date.split('/');
        const month = date[0];
        if(month < 3){
            item.Valid= false;
        }
        return item;
    }).sort((num1, num2)=>{
        const num1Array= num1.issue_date.split('/');
        const num2Array= num2.issue_date.split('/');

        const date1= Number(num1Array[0]);
        const date2= Number(num2Array[0]);
        const date3= Number(num1Array[1]);
        const date4= Number(num2Array[1]);


        if(date1 > date2){
            return 1;
        }else if(date1 < date2){
            return -1;
        }else if(date3 > date4){
            return 1;
        }else {
            return -1;
        }
    })


    return newAdd;
    
}
console.log(newField());
//console.log(items);
